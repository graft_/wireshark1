Wireshark1.py is a program that takes in a DPKT object from a pcap file to process and store it. Code to intake and access the pcap file and dpkt object was provided.
I grouped the DPKT objects by IP, port, and IP/port.
Then sorted them in descending order by number of packets.
Each iteration of the for loop counts the number of packets.
Then during each iteration I process each DPKT file to check the IP address, then check to see if there is a TCP port that is being accessed.
If there is a TCP port being accessed, I check if there is a value in ip.data and to see if it is an instance of dpkt.tcp.TCP.
If it is a new IP address I add it into the list then initialize the data to 1 and set the key to the IP address.
If a port is being accessed then I check if it is new or not.
When storing the IP/TCP when a TCP port is accessed for the first time, I set the key to 'IPaddress' + ':' + str(tcp.dport) and set the data to 1.
For any IP address/ port if it is new then add the port number as a key then set data to 1, if it isn't it just increments the port count by 1 of the respective port.