#!/usr/bin/python
# 
# This is the skeleton of the CS 352 Wireshark Assignment 1
#
# (c) 2018, R. P. Martin, GPL version 2

# Given a pcap file as input, you should report:
#
#1) number of the packets (use number_of_packets), 
#2) list distinct source IP addresses and number of packets for each IP address, in descending order 
#3) list distinct destination TCP ports and number of packers for each port(use list_of_tcp_ports, in descending order)
#4) The number of distinct source IP, destination TCP port pairs, in descending order 

import dpkt
import socket
import argparse 
import operator

from collections import OrderedDict

# this helper method will turn an IP address into a string
def inet_to_str(inet):
    # First try ipv4 and then ipv6
	try:
		return socket.inet_ntop(socket.AF_INET, inet)
	except ValueError:
		return socket.inet_ntop(socket.AF_INET6, inet)

# main code 
def main():
	number_of_packets = 0             # you can use these structures if you wish 
	list_of_ips = dict()
	list_of_tcp_ports = dict()
	list_of_ip_tcp_ports = dict()

	# parse all the arguments to the client 
	parser = argparse.ArgumentParser(description='CS 352 Wireshark Assignment 1')
	parser.add_argument('-f','--filename', help='pcap file to input', required=True)

	# get the filename into a local variable
	args = vars(parser.parse_args())
	filename = args['filename']

    # open the pcap file for processing 
	input_data=dpkt.pcap.Reader(open(filename,'r'))
	
    # this main loop reads the packets one at a time from the pcap file
	for timestamp, packet in input_data:
        # ... your code goes here ...
		number_of_packets += 1
		
		eth = dpkt.ethernet.Ethernet(packet)	# stores packet as ethernet
		
		# urls_d = Counter(list_of_urls)
		# figure out how to add these values to the dictionary
		# every TCP port comes from an IP but not every IP has to have a TCP port
		# inet_to_str(ip.src)
		
		# IP addresses, count
		if isinstance(eth.data, dpkt.ip.IP):
			ip = eth.data
			strip = inet_to_str(ip.src)
			if strip in list_of_ips:
				list_of_ips[strip] += 1
			else:
				list_of_ips.update({strip:1})
			
		# TCP ports, count
			# if ip.data exists
			if isinstance(ip.data, dpkt.tcp.TCP):
				tcp = ip.data
				
				# was getting incorrect numbers and realized that we only needed to search for dports
				# tcp.sport = source port
				# if tcp.sport is None:
				# 	continue
				# else:
				# 	if tcp.sport in list_of_tcp_ports: # if the sport is in the list of tcp ports
					# 	list_of_tcp_ports[tcp.sport] += 1	# if it is just increment
						
						# check when ip is the same but diff tcp port
						# if strip + ':' + str(tcp.dport) in list_of_ip_tcp_ports:
						# 	list_of_ip_tcp_ports[strip + ':' + str(tcp.sport)] += 1
						# else:
						# 	list_of_ip_tcp_ports.update({strip + ':' + str(tcp.sport):1})
						
					# else:	# if it isn't update the tcp port dict and the ip tcp port dict
						# list_of_tcp_ports.update({tcp.sport:1})
						# list_of_ip_tcp_ports.update({strip + ':' + str(tcp.sport):1})
					
				# tcp.dport = destinaion port
				if tcp.dport is None:
					continue
				else:
					if tcp.dport in list_of_tcp_ports:
						list_of_tcp_ports[tcp.dport] += 1
						
						if strip + ':' + str(tcp.dport) in list_of_ip_tcp_ports:
							list_of_ip_tcp_ports[strip + ':' + str(tcp.dport)] += 1
						else:
							list_of_ip_tcp_ports.update({strip + ':' + str(tcp.dport):1})
					else:
						list_of_tcp_ports.update({tcp.dport:1})
						list_of_ip_tcp_ports.update({strip + ':' + str(tcp.dport):1})
				
							
		# IP address/ TCP ports (both IP and TCP)
			# IP address and port and count
		pass	
			
	# sort dictionaries
	sorted_ips = sorted(list_of_ips.items(), key=operator.itemgetter(1), reverse=True)
	sorted_tcp_ports = sorted(list_of_tcp_ports.items(), key=operator.itemgetter(1), reverse=True)
	sorted_ips_tcp_ports = sorted(list_of_ip_tcp_ports.items(), key=operator.itemgetter(1), reverse=True)
		
	print "CS 352 Wireshark Michael Cheng, part 1"
	print "Total number of packets, " + str(number_of_packets)
	print "Source IP addresses, count"
	for key,value in sorted_ips:
		print key + "," + str(value)
	print "Destination TCP ports, count"
	for key,value in sorted_tcp_ports:
		print str(key) + "," + str(value)
	print "Source IPs/Destination TCP ports, count"
	for key,value in sorted_ips_tcp_ports:
		print key + "," + str(value)
	
	
# execute a main function in Python
if __name__ == "__main__":
	main()     